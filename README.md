# BCS Graphics

This project is boilerplate code in the C language to get a blank window up and running on the screen.

# Can I Use This?

Absolutely!

As long as you leave the license in the same folder.

If that bothers you, then please, don't use this code.

## Dependencies
1. OpenGL
2. GLEW
3. SDL2

## Compilation

`make all`

Or for using the address sanitizer available on linux:

`make debug`
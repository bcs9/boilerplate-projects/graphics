#ifndef __BCS_GRAPHICS__
#define __BCS_GRAPHICS__

/* Includes */
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>

#include <stdint.h>
#include <stdio.h>

/* Types */
typedef enum BCS_graphics__et {
    // none
    BCS_graphics__et_no_error,

    // file loading
    BCS_graphics__et_unable_to_open_file,
    BCS_graphics__et_file_larger_than_2GB,

    // random file loading
    BCS_graphics__et_unable_to_open_dev_urandom,

    // graphics initialization
    BCS_graphics__et_sdl2_initialization_failure,
    BCS_graphics__et_sdl2_window_opening_failure,
    BCS_graphics__et_sdl2_context_creation_failure,
    BCS_graphics__et_glew_initialization_failure,

    // shader compilation
    BCS_graphics__et_invalid_vertex_shader_source,
    BCS_graphics__et_invalid_fragment_shader_source,
    BCS_graphics__et_vertex_compilation_failed,
    BCS_graphics__et_fragment_compilation_failed,
    BCS_graphics__et_shader_linking_failed,

    // unknown
    BCS_graphics__et_unknown_error
} BCS_graphics__et;

typedef struct BCS_graphics__shaders {
    GLuint p_program_ID;
    char* p_vertex_file_address;
    char* p_fragment_file_address;
    char* p_vertex_file;
    char* p_fragment_file;
    GLuint p_vertex_ID;
    GLuint p_fragment_ID;
} BCS_graphics__shaders;

typedef struct BCS_graphics__graphics {
    SDL_Window* p_window;
    SDL_GLContext* p_context;
    unsigned char p_sdl2_initialization;
} BCS_graphics__graphics;

/* Library Functions */
char* BCS_graphics__string_copy(char* input) {
    char* output;
    unsigned long long length = 0;

    while (input[length] != 0) {
        length++;
    }

    output = (char*)malloc(sizeof(char) * length);

    for (unsigned long long i = 0; i < length; i++) {
        output[i] = input[i];
    }

    return output;
}

char* BCS_graphics__load_file(BCS_graphics__et* error, char* file_path) {
    char* output = 0;
    long long length = 0;
    FILE* f;

    // open the file
    f = fopen(file_path, "rb");

    // get file length
    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        rewind(f);
    } else {
        *error = BCS_graphics__et_unable_to_open_file;
        return 0;
    }

    // check if file is too large
    if (length > 2147483647) { // subtracted 1 for null termination
        fclose(f);
        
        *error = BCS_graphics__et_file_larger_than_2GB;

        return 0;
    }

    // create output
    output = (char*)malloc(sizeof(char) * (length + 1));

    // read file into memory
    length = fread(output, 1, length, f);

    // close file
    fclose(f);

    // write the null termination
    output[length] = 0;

    // return the binary buffer
    return output;
}

/* Error Handling */
void BCS_graphics__print_errors(BCS_graphics__et main_error, BCS_graphics__et sub_error) {
    printf("Error -> Main Error Code: %i; Sub Error Code: %i;\n", main_error, sub_error);

    return;
}

/* Shaders */
void BCS_graphics__destroy_shaders(BCS_graphics__shaders* shaders) {
    // free gpu resources
    glDeleteShader(shaders->p_vertex_ID);
    glDeleteShader(shaders->p_fragment_ID);
    glDeleteProgram(shaders->p_program_ID);

    // free cpu resources
    free(shaders->p_vertex_file_address);
    free(shaders->p_fragment_file_address);

    if (shaders->p_vertex_file != 0) {
        free(shaders->p_vertex_file);
    }
    if (shaders->p_fragment_file != 0) {
        free(shaders->p_fragment_file);
    }

    return;
}

BCS_graphics__shaders BCS_graphics__create_shaders(BCS_graphics__et* main_error, BCS_graphics__et* sub_error, char* vertex_file_address, char* fragment_file_address) {
    BCS_graphics__shaders output;
    GLint success;

    // zero output in case of error
    output.p_fragment_file = 0;
    output.p_fragment_file_address = 0;
    output.p_fragment_ID = 0;
    output.p_program_ID = 0;
    output.p_vertex_file = 0;
    output.p_vertex_file_address = 0;
    output.p_vertex_ID = 0;

    // initialize values
    output.p_program_ID = glCreateProgram();
    output.p_vertex_file_address = BCS_graphics__string_copy(vertex_file_address);
    output.p_fragment_file_address = BCS_graphics__string_copy(fragment_file_address);
    output.p_vertex_ID = glCreateShader(GL_VERTEX_SHADER);
    output.p_fragment_ID = glCreateShader(GL_FRAGMENT_SHADER);

    // open files
    output.p_vertex_file = BCS_graphics__load_file(sub_error, vertex_file_address);
    if (*sub_error != BCS_graphics__et_no_error) {
        *main_error = BCS_graphics__et_invalid_vertex_shader_source;

        return output;
    }

    output.p_fragment_file = BCS_graphics__load_file(sub_error, fragment_file_address);
    if (*sub_error != BCS_graphics__et_no_error) {
        *main_error = BCS_graphics__et_invalid_vertex_shader_source;

        return output;
    }

    // send programs to gpu
    glShaderSource(output.p_vertex_ID, 1, (const GLchar* const*)&(output.p_vertex_file), NULL);
    glShaderSource(output.p_fragment_ID, 1, (const GLchar* const*)&(output.p_fragment_file), NULL);

    // compile shaders
    glCompileShader(output.p_vertex_ID);
    glCompileShader(output.p_fragment_ID);

    // check for compilation errors
    glGetShaderiv(output.p_vertex_ID, GL_COMPILE_STATUS, &success);
    if (!success) {
        *main_error = BCS_graphics__et_vertex_compilation_failed;

        return output;
    }

    glGetShaderiv(output.p_fragment_ID, GL_COMPILE_STATUS, &success);
    if (!success) {
        *main_error = BCS_graphics__et_vertex_compilation_failed;

        return output;
    }

    // link shaders together
    glAttachShader(output.p_program_ID, output.p_vertex_ID);
    glAttachShader(output.p_program_ID, output.p_fragment_ID);

    glLinkProgram(output.p_program_ID);
    
    glGetProgramiv(output.p_program_ID, GL_LINK_STATUS, &success);
    if (!success) {
        *main_error = BCS_graphics__et_shader_linking_failed;

        return output;
    }

    // finished
    return output;
}

void BCS_graphics__use_shaders(BCS_graphics__shaders shaders) {
    glUseProgram(shaders.p_program_ID);

    return;
}

/* Graphics Library Handling */
void BCS_graphics__destroy_graphics(BCS_graphics__graphics* graphics) {
    // clean up window
    if (graphics->p_context != 0) {
        SDL_GL_DeleteContext(graphics->p_context);
    }
    if (graphics->p_window != 0) {
        SDL_DestroyWindow(graphics->p_window);
    }
    if (graphics->p_sdl2_initialization == 0) {
        SDL_Quit();
    }

    return;
}

BCS_graphics__graphics BCS_graphics__initalize_graphics(BCS_graphics__et* error, char* window_title, int window_width, int window_height) {
    BCS_graphics__graphics output;

    // zero output in case of error
    output.p_window = 0;
    output.p_context = 0;
    output.p_sdl2_initialization = 0;
    
    // initialize SDL2
    output.p_sdl2_initialization = 0;
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        *error = BCS_graphics__et_sdl2_initialization_failure;
        output.p_sdl2_initialization = 1;

        return output;
    }

    // create window
    output.p_window = SDL_CreateWindow(window_title, 0, 0, window_width, window_height, SDL_WINDOW_OPENGL);
    if (output.p_window == 0) {
        *error = BCS_graphics__et_sdl2_window_opening_failure;

        return output;
    }

    // create opengl context
    output.p_context = SDL_GL_CreateContext(output.p_window);
    if (output.p_context == 0) {
        *error = BCS_graphics__et_sdl2_context_creation_failure;

        return output;
    }

    // initialize glew
    if (glewInit() != GLEW_OK) {
        *error = BCS_graphics__et_glew_initialization_failure;

        return output;
    }

    // graphics are up and running
    return output;
}

/* Events */
unsigned char BCS_graphics__should_quit() {
    SDL_Event e;
    SDL_Event* ep = &e;
    GLenum error;
    
    // get events
    while (SDL_PollEvent(ep)) {
        if (e.type == SDL_QUIT) {
            return 1;
        }
    }

    // get opengl errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        printf("Opengl Error Code: %i\n", error);
        return 1;
    }

    return 0;
}

#endif
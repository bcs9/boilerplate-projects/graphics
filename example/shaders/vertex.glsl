/*
Author: bshapleigh
GitLab: https://gitlab.com/bshapleigh
 */

#version 330 core

layout (location = 0) in vec2 pass_position;

void main() {
    gl_Position = vec4(pass_position, 0.0f, 1.0f);
}
#include "../BCS_graphics.h"

/* Main */
int main() {
    BCS_graphics__et main_error = BCS_graphics__et_no_error;
    BCS_graphics__et sub_error = BCS_graphics__et_no_error;
    BCS_graphics__graphics graphics;
    BCS_graphics__shaders shaders;

    // startup message
    printf("Starting BCS_graphics!\n");
    
    // initialize graphics
    graphics = BCS_graphics__initalize_graphics(&main_error, "BCS_graphics!", 720, 480);
    if (main_error != BCS_graphics__et_no_error) {
        goto BCS_graphics__label_destroy_graphics;
    }

    // initalize shaders
    shaders = BCS_graphics__create_shaders(&main_error, &sub_error, (char*)"./shaders/vertex.glsl", (char*)"./shaders/fragment.glsl");
    if (main_error != BCS_graphics__et_no_error || sub_error != BCS_graphics__et_no_error) {
        goto BCS_graphics__label_destroy_shaders;
    }

    // use shaders
    BCS_graphics__use_shaders(shaders);

    // setup opengl drawing states
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    while (!BCS_graphics__should_quit()) {
        // clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // update screen
        SDL_GL_SwapWindow(graphics.p_window);
    }

    BCS_graphics__label_destroy_shaders: BCS_graphics__destroy_shaders(&shaders);
    BCS_graphics__label_destroy_graphics: BCS_graphics__destroy_graphics(&graphics);

    printf("Quitting BCS_graphics and logging errors!\n");

    if (main_error != BCS_graphics__et_no_error || sub_error != BCS_graphics__et_no_error) {
        BCS_graphics__print_errors(main_error, sub_error);

        return 1;
    }

    return 0;
}